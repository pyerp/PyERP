<a name="8.0.0.rc21"></a>
## 8.0.0.rc21 (2019-01-08)




<a name="8.0.0.rc19"></a>
## 8.0.0.rc19 (2018-11-01)


#### Feature

* **marketing_campaign:**  trae módulo dese la versión 8 ([86da634d](86da634d))

#### Bug Fixes

* **fields:**  permite utilizar indistintamente index o select para establecer un campo como indice ([6c451257](6c451257))



<a name="8.0.0.rc18"></a>
## 8.0.0.rc18 (2018-10-26)


#### Feature

* **account_report_company:**  elimina módulo obsoleto ([451311fd](451311fd))
* **barcodes:**  copia módulo de la versión 9 de odoo ([0f6a93c8](0f6a93c8))

#### Bug Fixes

* **barcodes:**  adapta el módulo para funcionar en la versión 8 ([2d98ba96](2d98ba96))



<a name="8.0.0.rc17"></a>
## 8.0.0.rc17 (2018-10-19)




<a name="8.0.0.rc16"></a>
## 8.0.0.rc16 (2018-10-08)


#### Bug Fixes

* **account_report_company:**  deprecia autoinstalación ([efc0fd86](efc0fd86))

#### Feature

* **board:**  agrega módulo desde la versión 8.0 ([b742a373](b742a373))
* **calendar:**  agrega módulo calendar ([6b3341ea](6b3341ea))
* **google_account:**  agrega módulo desde la versión 8.0 ([d159c9d8](d159c9d8))
* **hr_evaluation:**  agrega módulo desde la versión 8.0 ([6d111e20](6d111e20))
* **hr_holidays:**  agrega módulo desde la versión 8.0 ([bbeb5860](bbeb5860))



<a name="8.0.0.rc15"></a>
## 8.0.0.rc15 (2018-08-23)


#### Feature

* **bus:**  add module from odoo 8.0 ([930bda9f](930bda9f))
* **im_chat:**  agrega módulo desde version 8.0 ([06c16117](06c16117))
* **payment:**  agrega pasarelas de pago desde versión 8.0 ([5f37a25b](5f37a25b))

#### Bug Fixes

* **bus:**  backport patch that add compatibility with external long polling handler ([73df426a](73df426a))



<a name="8.0.0.rc14"></a>
## 8.0.0.rc14 (2018-06-13)


#### Bug Fixes

* **contacts:**  updatemo module to version 8 ([c1433c23](c1433c23))
* **document:**
  *  restaura el menú de acceso a los documentos ([4125b78c](4125b78c))
  *  update module to version 8.0 ([a4902a5b](a4902a5b))
* **document_page:**  remove obsolete module ([cabb09ad](cabb09ad))
* **es_MX:**  actualiza la traducción ([9d830602](9d830602))
* **gamification:**  oculta botones del objetivo para los usuarios ([faaf569a](faaf569a))
* **graph:**  Se establece por defecto la grafica stacket ([c32df071](c32df071))
* **knowledge:**  update module to v8 ([1df36b58](1df36b58))
* **payment:**  deshabilita la instalación por defecto del módulo ([815aec08](815aec08))
* **payment_paypal:**  actualiza la plantilla del formulario de pago ([1be13c52](1be13c52))
* **portal:**
  *  elimina menus que no se usan ([43eb175d](43eb175d))
  *  deshabilita los módulos del portal ([364871a9](364871a9))
  *  update to module version 8 ([71326741](71326741))
* **portal_sale:**  corrije los permisos de usuario ([0fe1537c](0fe1537c))
* **website:**
  *  deshabilita la página que muestra la información del sistema ([8847e3d5](8847e3d5), closes [#18](18))
  *  agrega un menu para permitir configurar el sitio web ([1106c5bc](1106c5bc))

#### Features

* **gamification:**  bring module from odoo 8 ([d464a691](d464a691))
* **payment_paypal:**  agrega el módulo desde odooo v8 ([45369bef](45369bef))
* **payment_transfer:**  agrega el módulo desde odoo 8 ([94810ffb](94810ffb))
* **web_kanban_gauge:**  add module from odoo 8.0 ([94552c4e](94552c4e))



<a name="8.0.0.rc13"></a>
## 8.0.0.rc13 (2018-05-26)


#### Bug Fixes

* **graph:**  Se establece por defecto la grafica stacket ([c32df071](c32df071))



<a name="8.0.0.rc12"></a>
## 8.0.0.rc12 (2018-02-21)




<a name=""></a>
##  (2016-06-01)


#### Features

* **account:**
  *  add migration script for rename rate column ([b3e67eb3](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/b3e67eb33623a0064e917a01eb6685646bebc21a))
  *  add migration script for rename rate column ([64560065](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/64560065ecd394c6cf143851a7654815f712bc7d))
* **message:**  remove phoning home features ([023000ef](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/023000ef68dd6dc486dc20e6ceb6b9f6ab1d1902))
* **migrations:**  add migrations ([1b872888](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/1b872888edc2094c2a0341ce4b6a0d208bc291d6))
* **pyerp/pyerp:**
  *  Agreagted context to onchange_partner_id ([5ed54352](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/5ed5435224a1c45d48f6f5a289376733f81b8577))
  *  Agreagted context to onchange_partner_id ([c787ca6e](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/c787ca6ed9d64bcbc8515870acfc9b9330633265))
* **stock:**
  *  Added fields history and followers in stock.picking ([76b1cf75](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/76b1cf752c019f0676fea2a1bd4247dac53e4740))
  *  move track lot related fields from product.product to product.template ([51c06648](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/51c066482926e58029980463fc28f1769b76b5bb))
* **web:**  Update web modules ([9d46f25c](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/9d46f25c5a7c1cb9fd4c207310022e11a7d53cb0))

#### Bug Fixes

*   fiscal position ([f996e84c](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/f996e84c6b660da03588a56a7144a9b8097bfdc8))
*  Displayed required and readonly attributes of a model. ([68dd2596](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/68dd2596bf3f24894da245ffe4c219d4484f0f9a))
*   major bugs but still it not properly evaluate soem mails for recursive multipart ([a012a084](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/a012a084fca239df619d10c01abb8631ae59da89))
*   major bugs but still it not properly evaluate soem mails for recursive multipart ([be8fc451](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/be8fc4511cb1637afc051ac4a48ad2a60687e925))
* **Account:**  Currency rate where inverted on the onchange functions resulting on a journal with bad ([fc3050cb](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/fc3050cb0f0f75e6a3f7b4b01ea692eb7f0831b3))
* **account:**
  *  Revert commits that upgrade functions from account.invoice to new api ([385a4a1c](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/385a4a1c287d30e46a1df433d5f79258a21cfe94))
  *  migrate function that compute taxes to new api ([f1055af2](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/f1055af2848f60cd90b52eabe4b692f7c6ef8229))
  *  migrate function compute_all to new api ([9f69dd81](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/9f69dd81f63f5c624852d9e591ddf0ff19a9127e))
  *  add compute with v7 decorator for back compatibility ([bde8815c](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/bde8815c8b33c2a0d3837537801958a9eb92a842))
  *  migrate function that computes taxes to new api ([42296290](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/42296290cfcca0115b6a4acc28be392d2bd6ffd0))
  *  account_invoice validate related funcionts update to new api ([645471d5](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/645471d57aca67f16f48eb3032c348ef775bfb54))
  *  fix migration script for version 1.1.1 ([9e5d1fa5](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/9e5d1fa5996f7e426e1366f770832cbc3835ba3d))
* **account_invoice:**
  *  When creating an invoice from sales or purchase order using different currency ([56b3ad60](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/56b3ad60bbf77d61a523d39bc25a4288b80b55f9))
  *  fix exception trought when creating new invoice ([347bd8c0](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/347bd8c0bc89141771dd49fd118484dfa38bace1))
* **account_move_line create:**  transform frozen dictionary into a normal dictionary ([2de3f3de](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/2de3f3de4af2f010c727310d6d185e7e20bfe2da))
* **account_voucher:**  Add name field for account_vocher_line on view form to save changes generated ([3e6e77e9](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/3e6e77e9b2d6273e65be49927f4f9ae0a2de43c4))
* **base:**  fix base 2.0.0 migration ([06bcd435](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/06bcd4356decb8f0c656e16b5a6a593f87530f1f))
* **base/ir/ir_actions:**
  *  Changes in ir_actions for sending mail with template ([87d38f7e](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/87d38f7e0dbd08db25c575f2dc21e2a6b14d5657))
  *  Added mail subtype ([1f049a1b](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/1f049a1b8d65518d3ab4facba8c757d5ae9a98f9))
* **currency rate:**
  *  it is not possible to set a currency rate equal to zero ([a02b66d7](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/a02b66d7a15ba4a13aa66e149908cfd9e2edab4a))
  *  missing label string in field currency_rate ([15e29327](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/15e29327b656578b843e1ebca512fb5c3158484c))
* **ir_model:**  replace pooler with register ([e19aea4a](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/e19aea4afdf0327e067d88308be55685138aac65))
* **mail:**  get back class contract mail updater to prevent failitures ([70ae4c6c](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/70ae4c6cc5f37f21da202378053a8e6c5f110388))
* **merge request:**  fix last merge request ([71a34488](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/71a34488c2dca15df0b7f268469b64f38e17aa9f))
* **merge request conflicts:**  fix merge request conflicts ([73199466](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/73199466fe83922f19b265b921d6f8c694f64878))
* **migrations:**
  *  fixed migration as recomended on openuprade project ([7b4f8a4c](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/7b4f8a4c0b5d349de45af9e5d7f4e8b10d777fc4))
  *  add missing migrations for base module ([4137c389](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/4137c38950006af88652b61149b76c836f403ffe))
* **mrp:**  Added signals in button Cancel production ([420bb5fd](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/420bb5fd016d96d2277218d8c421b7716098da63))
* **mrp lot:**  fix constraint when it is selected track production but it is not passed and lot_id ([1b73d6c2](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/1b73d6c21713d191434da14749c7ad6c589146d9))
* **portal:**  not object variable send to render the mail template ([301dec10](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/301dec1078db6f15613b28119470621d88cc1dc4))
* **project:**  several changes for make project module work with new api ([24a43dc2](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/24a43dc2f7878a5ca2533e2b9e902f06f07850d6))
* **pyerp/Pyerp:**  Completed function _get_eval_context ([40cbabb2](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/40cbabb2cc1f59160734050f1ed994b451752608))
* **pyerp/account:**
  *  Change field multi currency, added on_change invoice_date ([e4d5e2bc](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/e4d5e2bc8eed66f3aaa9e2f9e0214e71cf9179e2))
  *  error to agregate new account move line in journal entry ([050c0aec](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/050c0aecfb33b2f92265c78e9a64fd2e1a522297))
* **report:**  remove extra _ from report that were preventing registering new reports ([9090083f](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/9090083fbe329ce026c2e08531fc5c407fc1aa30))
* **report_webkit:**
  *  upgrade to new api on version ([0514320e](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/0514320e3823f71bebc3f9c7579f8a71e0db1635))
  *  the registering function was passing incorrect parser to the Report class ([8056dfcb](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/8056dfcb4c645b34fddaf6a04147b80be4cc241c))
* **reports:**  rewrite existing report ([2394e0e2](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/2394e0e280a5e7ed3059d5888c89617c8408b844))
* **resource:**  set correctly prioroty in the menu_id ([d463c988](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/d463c988d1a7899b877137fe18f978136ee6506f))
* **sale:**  prevent warning about price list change display when creating a new sale order ([1179ead6](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/1179ead665134b52f63f73fe170ae2cd2f78272b))
* **setup:**  add mising dependencies ([9f0b5ec4](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/9f0b5ec4de8888c265ffe9924200acb34a7860b8))
* **stock:**
  *  Enforce lot on internal moves for products with track_all set to True ([3943bc6e](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/3943bc6e4328fbb8ea28d8e3162e03bc71e21d74))
  *  fixed queries on stock 1.2.0 migrations ([0a23de4e](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/0a23de4e4fa4ece23834683635c83f9f3cd4cd84))
* **stock.py:**  stock.move is created without lot_id nonetheless it is marked as required ([aa162ebe](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/aa162ebef070c0a32d294e89d7503487c6728aa1))
* **stock_location:**  remove obsolete test files ([46759baa](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/46759baaea294f7755b1d36550568606cf5dce47))
* **stock_move:**  upgrade stock.move product_lot coinstraint to new api ([a98bffdb](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/a98bffdba18f0b7189648a32d89980451b1002e4))
* **stock_return_picking:**  only check the stock.moves in which their state be equal to done ([d4e0c726](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/d4e0c72654ef4163d765ca31452586116f383bf1))
* **translations:**  Set cache multi for get correct translations ([081c6cbe](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/081c6cbe5c99efffe9710493e5fd719ccbf041fd))
* **update:**  get back update class ([8784f9d1](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/8784f9d15b2f56ba7c5dbeab5fcf8d8aded962ec))
* **web:**
  *  Compare values in onchange using === operator ([5ceb3a6d](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/5ceb3a6dba1e6102c4b689151857a9e620c2d9e6))
  *  fix for error TypeError: session_info() takes exactly 2 arguments (1 given) ([5ced8ab8](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/5ced8ab80ee4675a8dba2d2344ae6c1a2569afb8))
  *  fix problem with about window not oppening ([60cb39b0](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/60cb39b08af6fbd9308c61cd98c7a65228557c19))
* **webkit_report.py:**  match branch 7.0 with PyERP branch ([f5a494e5](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/f5a494e59127696a77c22d37dc6c8ee21b926fed))

#### Performance

* **account.py:**  remove deprecated pooler.get_pool ([91b06bf8](http://gitlab.openpyme.mx/pyerp/PyERP.git/commit/91b06bf83372c667988a376779eb4f4ba57b98c5))



