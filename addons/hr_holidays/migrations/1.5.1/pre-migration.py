# -*- coding: utf-8 -*-

from openupgradelib import openupgrade as tools


@tools.migrate()
def migrate(cr, version):
    cr.execute(
        "ALTER TABLE hr_holidays DROP CONSTRAINT IF EXISTS "
        "hr_holidays_meeting_id_fkey"
    )
    cr.execute(
        '''update hr_holidays
        set meeting_id=calendar_event.id
        from calendar_event where meeting_id=%s''' % (
            tools.get_legacy_name('crm_meeting_id'),
        )
    )
