# -*- coding: utf-8 -*-

from openupgradelib import openupgrade as tools

from openerp import pooler, SUPERUSER_ID


@tools.migrate()
def migrate(cr, version):
    pool = pooler.get_pool(cr.dbname)
    interview_obj = pool['hr.evaluation.interview']
    survey_id = pool['ir.model.data'].xmlid_to_res_id(
        cr, SUPERUSER_ID,
        'hr_evaluation.appraisal_form',
        raise_if_not_found=True
    )
    for interview_id in interview_obj.search(cr, SUPERUSER_ID, [], context={}):
        interview = interview_obj.browse(
            cr, SUPERUSER_ID, interview_id, context={}
        )
        interview.request_id.write({"survey_id": survey_id})
