# -*- coding: utf-8 -*-

from openupgradelib import openupgrade as tools
from openerp import pooler, SUPERUSER_ID


@tools.migrate()
def migrate(cr, version):
    pool = pooler.get_pool(cr.dbname)
    execute = tools.logged_query
    user_input_obj = pool['survey.user_input']
    execute(cr, "SELECT id FROM hr_evaluation_interview")
    for interview_id in cr.fetchall():
        ret = user_input_obj.create(cr, SUPERUSER_ID, {
            'survey_id': 1,
            'type': 'link',
        }, context={})
        execute(
            cr,
            "UPDATE hr_evaluation_interview\n"
            "SET request_id = %d WHERE id = %d\n" %
            (ret, interview_id[0])
        )
