# -*- coding: utf-8 -*-

from openupgradelib import openupgrade as tools


column_change_type = [
    ('hr_job', 'expected_employees'),
    ('hr_job', 'no_of_employee'),
    ('hr_job', 'no_of_recruitment')
]


@tools.migrate()
def migrate(cr, version):
    for table, field in column_change_type:
    tools.float_to_integer(cr, table, field)

