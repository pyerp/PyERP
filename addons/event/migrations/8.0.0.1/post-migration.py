# -*- coding: utf-8 -*-

from openupgradelib import openupgrade as tools


@tools.migrate()
def migrate(cr, version):
    pool = pooler.get_pool(cr.dbname)
    convert_field_to_html(
        cr, 'event_event', get_legacy_name('note'),
        'description'
    )

