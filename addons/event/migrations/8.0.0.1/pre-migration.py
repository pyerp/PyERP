# -*- coding: utf-8 -*-

from openupgradelib import openupgrade as tools


column_renames = {
    'event_event': [
        ('note', None),
    ],
}


@tools.migrate()
def migrate(cr, version):
    tools.rename_columns(cr, column_renames)

