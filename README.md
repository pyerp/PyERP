<a name="8.0.0.rc7"></a>
## 8.0.0.rc7 (2017-03-10)


#### Features

* **account:**
  *  add migration script for rename rate column ([b3e67eb3](b3e67eb3))
  *  add migration script for rename rate column ([64560065](64560065))
* **account_invoice:**  Add a confirm message before canceling an invoice ([612415b0](612415b0))
* **account_widget js:**  add new file account_widget.js ([8eda65d4](8eda65d4))
* **addons:**  Bundle addons for include css and js onto web template ([e5c4e768](e5c4e768))
* **crm board:**  fix display in board menu of crm ([cbeca6be](cbeca6be))
* **message:**  remove phoning home features ([023000ef](023000ef))
* **migrations:**
  *  add email_template migrations ([c39c6376](c39c6376))
  *  add migrations ([1b872888](1b872888))
* **pyerp/pyerp:**
  *  Agreagted context to onchange_partner_id ([5ed54352](5ed54352))
  *  Agreagted context to onchange_partner_id ([c787ca6e](c787ca6e))
* **report:**  Add report module ([37c143e4](37c143e4))
* **stock:**
  *  change the workflow in which sotck moves are received ([513513ca](513513ca))
  *  Added fields history and followers in stock.picking ([76b1cf75](76b1cf75))
  *  move track lot related fields from product.product to product.template ([51c06648](51c06648))
* **stock.location.lot:**  add report in stock.location.lot with barcode and lot name ([32207192](32207192))
* **view_form.js:**  add widget percent ([eb7b4ac5](eb7b4ac5))
* **web:**  Update web modules ([9d46f25c](9d46f25c))

#### Performance

* **account.py:**  remove deprecated pooler.get_pool ([91b06bf8](91b06bf8))
* **share module:**  first step to deprecate share module ([8069ca05](8069ca05))

#### Bug Fixes

*   fiscal position ([f996e84c](f996e84c))
*  Displayed required and readonly attributes of a model. ([68dd2596](68dd2596))
*   major bugs but still it not properly evaluate soem mails for recursive multipart ([a012a084](a012a084))
*   major bugs but still it not properly evaluate soem mails for recursive multipart ([be8fc451](be8fc451))
* **Account:**  Currency rate where inverted on the onchange functions resulting on a journal with bad ([fc3050cb](fc3050cb))
* **__openerp__.py:**  add javascript ([48e8c892](48e8c892))
* **account:**
  *  Revert commits that upgrade functions from account.invoice to new api ([385a4a1c](385a4a1c))
  *  migrate function that compute taxes to new api ([f1055af2](f1055af2))
  *  migrate function compute_all to new api ([9f69dd81](9f69dd81))
  *  add compute with v7 decorator for back compatibility ([bde8815c](bde8815c))
  *  migrate function that computes taxes to new api ([42296290](42296290))
  *  account_invoice validate related funcionts update to new api ([645471d5](645471d5))
  *  fix migration script for version 1.1.1 ([9e5d1fa5](9e5d1fa5))
* **account.invoice:**
  *  Get default value for currency rate according with current rate and selected c ([2c7d881d](2c7d881d))
  *  Add iteration on is_multi_currency function to prevent crash when multiple inv ([a266d59c](a266d59c))
  *  rename parameter to fnames ([d477bc3c](d477bc3c))
* **account.voucher:**  only select journals that belong to cash or bank type ([7aac438a](7aac438a))
* **account_analytic_plans:**  Remove deprecated form view on account.bank.statement.line ([3af95b73](3af95b73))
* **account_invoice:**
  *  Fix the blank spaces in field description ([cc483403](cc483403))
  *  test type of accounts to get residual amount ([39d90d32](39d90d32))
  *  test type of accounts to get residual amount ([a1efb602](a1efb602))
  *  clean cache to read correctly field residual ([6d0903d3](6d0903d3))
  *  fix onchange_company_id ([4c3c6685](4c3c6685))
  *  fix last merge ([b8cf1317](b8cf1317))
  *  use amount residual as testing to advance an invoice to paid state ([2477271e](2477271e))
  *  When creating an invoice from sales or purchase order using different currency ([56b3ad60](56b3ad60))
  *  fix exception trought when creating new invoice ([347bd8c0](347bd8c0))
* **account_invoice_refund:**  frozen dictionary ([f26ecd95](f26ecd95))
* **account_move_line create:**  transform frozen dictionary into a normal dictionary ([2de3f3de](2de3f3de))
* **account_voucher:**
  *  unfrozen frozen dict ([d1c2c508](d1c2c508))
  *  Add name field for account_vocher_line on view form to save changes generated ([3e6e77e9](3e6e77e9))
* **base:**
  *  apply path for 7a01d4a commit from OCB ([67c327df](67c327df))
  *  fix base 2.0.0 migration ([06bcd435](06bcd435))
* **base migration:**  rename column only if exist ([1261ee56](1261ee56))
* **base/ir/ir_actions:**
  *  Changes in ir_actions for sending mail with template ([87d38f7e](87d38f7e))
  *  Added mail subtype ([1f049a1b](1f049a1b))
* **board.js:**  add and update board.js ([4ad3eb29](4ad3eb29))
* **crm.make.sale:**  fix call to message_post function ([dd67e3b6](dd67e3b6))
* **currency rate:**
  *  it is not possible to set a currency rate equal to zero ([a02b66d7](a02b66d7))
  *  missing label string in field currency_rate ([15e29327](15e29327))
* **document_ftp and document_webdav:**  fix imports ([99d980a1](99d980a1))
* **document_webdav:**  remove deprecated attribute in field ([9525ba28](9525ba28))
* **email_template:**  odoo 8 only allows send a template is this is related with a model ([77559243](77559243))
* **hr_payroll:**
  *  Human Resources Officer group must be able to see all payslips ([83f9bbd5](83f9bbd5))
  *  Company id for payslip were being set to null when employee works for several compa ([da00cf58](da00cf58))
* **hr_payroll_account:**  Add missing context to avoid frozendict error ([d83b1542](d83b1542))
* **hr_timesheet:**  Remove deprecated nodes on demo data ([f645006c](f645006c))
* **ir_model:**  replace pooler with register ([e19aea4a](e19aea4a))
* **mail:**  get back class contract mail updater to prevent failitures ([70ae4c6c](70ae4c6c))
* **mail.mail:**  unfreeze dictionary ([cf366b46](cf366b46))
* **mail.thread:**  Chang reference to read field that now is named is_read ([585bad3c](585bad3c))
* **merge request:**  fix last merge request ([71a34488](71a34488))
* **merge request conflicts:**  fix merge request conflicts ([73199466](73199466))
* **migrations:**
  *  fixed migration as recomended on openuprade project ([7b4f8a4c](7b4f8a4c))
  *  add missing migrations for base module ([4137c389](4137c389))
* **mrp:**  Added signals in button Cancel production ([420bb5fd](420bb5fd))
* **mrp lot:**  fix constraint when it is selected track production but it is not passed and lot_id ([1b73d6c2](1b73d6c2))
* **point_of_sale:**  Allow point of sale from v8 to works properly on master ([6c57e98d](6c57e98d))
* **portal:**  not object variable send to render the mail template ([301dec10](301dec10))
* **procurement.order:**  create a new stock_move instead of writing the existing one ([08928487](08928487))
* **procurement.rule:**  Procurement rule set to properly create stock move for the rules configured l ([935e817d](935e817d))
* **procurement_order:**  use the new stock_move to generate new procurement ([cfebbf88](cfebbf88))
* **product.product:**  add correctly in the view track_all field ([baf53cdc](baf53cdc))
* **project:**  several changes for make project module work with new api ([24a43dc2](24a43dc2))
* **project_mrp:**  remove model from declaration ([f28e392b](f28e392b))
* **purchase:**  Fix taken translation ([4a85f145](4a85f145))
* **pyerp/Pyerp:**  Completed function _get_eval_context ([40cbabb2](40cbabb2))
* **pyerp/account:**
  *  Change field multi currency, added on_change invoice_date ([e4d5e2bc](e4d5e2bc))
  *  error to agregate new account move line in journal entry ([050c0aec](050c0aec))
* **report:**  remove extra _ from report that were preventing registering new reports ([9090083f](9090083f))
* **report_webkit:**
  *  upgrade to new api on version ([0514320e](0514320e))
  *  the registering function was passing incorrect parser to the Report class ([8056dfcb](8056dfcb))
* **reports:**  rewrite existing report ([2394e0e2](2394e0e2))
* **res_partner view:**  rename field name to display_name ([f9dcdd68](f9dcdd68))
* **resource:**  set correctly prioroty in the menu_id ([d463c988](d463c988))
* **sale:**  prevent warning about price list change display when creating a new sale order ([1179ead6](1179ead6))
* **setup:**  add mising dependencies ([9f0b5ec4](9f0b5ec4))
* **share.wizard:**
  *  give permissions to user of reading many2one objects ([fe521ab2](fe521ab2))
  *  share invoices, payroll etc and theirs documents ([ba538c50](ba538c50))
* **stock:**
  *  Move track_all field to product.product form view in order to render properly ([3d1998c7](3d1998c7))
  *  Enforce lot on internal moves for products with track_all set to True ([3943bc6e](3943bc6e))
  *  fixed queries on stock 1.2.0 migrations ([0a23de4e](0a23de4e))
* **stock module:**  remove remaining stock files ([f630506a](f630506a))
* **stock.picking:**
  *  update function name to call workflow ([89fdff3d](89fdff3d))
  *  add lazy parameter to read_group function ([b52e4aac](b52e4aac))
* **stock.py:**  stock.move is created without lot_id nonetheless it is marked as required ([aa162ebe](aa162ebe))
* **stock_location:**  remove obsolete test files ([46759baa](46759baa))
* **stock_move:**
  *  rename inherit button ([9d5efa65](9d5efa65))
  *  upgrade stock.move product_lot coinstraint to new api ([a98bffdb](a98bffdb))
* **stock_return_picking:**  only check the stock.moves in which their state be equal to done ([d4e0c726](d4e0c726))
* **tools:**  extract terms in correct folder ([284d9d69](284d9d69))
* **translations:**  Set cache multi for get correct translations ([081c6cbe](081c6cbe))
* **update:**  get back update class ([8784f9d1](8784f9d1))
* **web:**
  *  prevent multiple trigger in confirm popup ([d930c4dc](d930c4dc))
  *  Set width for sheet on form view ([f6c0860f](f6c0860f))
  *  Compare values in onchange using === operator ([5ceb3a6d](5ceb3a6d))
  *  fix for error TypeError: session_info() takes exactly 2 arguments (1 given) ([5ced8ab8](5ced8ab8))
  *  fix problem with about window not oppening ([60cb39b0](60cb39b0))
* **webkit_report.py:**  match branch 7.0 with PyERP branch ([f5a494e5](f5a494e5))



[![Build Status](https://travis-ci.org/OCA/OCB.png?branch=7.0)](https://travis-ci.org/OCA/OCB)
[![Coverage Status](https://coveralls.io/repos/OCA/OCB/badge.png?branch=7.0)](https://coveralls.io/r/OCA/OCB?branch=master)

[Original readme](README)
