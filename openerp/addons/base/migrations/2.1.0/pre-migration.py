# -*- coding: utf-8 -*-

from openupgradelib import openupgrade as tools
from openerp import SUPERUSER_ID


@tools.migrate(use_env=True, uid=SUPERUSER_ID)
def migrate(env, installed_version):
    tools.update_module_names(env.cr, [("base_calendar", "calendar")])
