"""

Revision ID: base_2.0.0
Revises:
Create Date: 2016-02-19 13:07:32.925776

"""
from openupgradelib import openupgrade as tools

# revision identifiers, used by Alembic.
revision = '2.0.0'
down_revision = '1.3'
branch_labels = None
depends_on = None

column_renames = {
    'res_company': [
        ('paper_format', 'rml_paper_format'),
    ],
}

rename_xml_ids = [
    ('portal.group_anonymous', 'base.group_public'),
    ('portal.group_portal', 'base.group_portal'),
]

@tools.migrate()
def migrate(cr, installed_version):
    if (
        not tools.column_exists(cr, 'res_company', 'paper_format') or
        tools.column_exists(cr, 'res_company', 'rml_paper_format')
    ):
        return
    tools.rename_columns(cr, column_renames)
    tools.rename_xmlids(cr, rename_xml_ids)
