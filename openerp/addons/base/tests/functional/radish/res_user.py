# -*- coding: utf-8 -*-

from openerp import api

from radish import given, when, then


@given('the user {username:w} exists')
def given_the_user_exists(step, username):
    # Get current company from environment
    company = step.context.env.user.company_id
    user = step.context.env['res.users'].create({
        'name': username,
        'login': username,
        'password': 'disabled',
        'company_id': company.id,
    })
    setattr(step.context, username, user)


@given('the user {username:w} has role {rolename:S}')
def given_the_user_has_role(step, username, rolename):
    user = getattr(step.context, username)
    role = step.context.env.ref(rolename)
    user.write({
        'groups_id': [(4, role.id)],
    })


@given('I log in as user {username:w}')
def given_i_log_in_as_user(step, username):
    user = getattr(step.context, username)
    # Reload the environment with the given user
    env = step.context.env
    step.context.env = env(user=user.id)


@given('{user:w} user is logged in with role {role:S}')
def given_user_is_loggedin_with_role(step, user, role):
    step.behave_like('Given the user %s exists' % user)
    step.behave_like('Given the user %s has role %s' % (user, role))
    step.behave_like('Given I log in as user %s' % user)
