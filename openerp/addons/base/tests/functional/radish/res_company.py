# -*- coding: utf-8 -*-

from radish import given, when, then


@given('new company named {company_name:w}')
def _new_company(step, company_name):
    CompanyObj = step.context.env['res.company']
    new_company = CompanyObj.create({'name': company_name})
    setattr(step.context, company_name, new_company)


@given('{company_name:w} company currency is {currency:w}')
def _set_company_currency(step, company_name, currency):
    # We need to search for proper currency
    currency = step.context.env['res.currency'].search(
        [('name', '=', currency)], limit=1,
    )
    if company_name == 'current':
        company = step.context.env.user.company_id
    else:
        company = getattr(step.context, company_name)
    company.write({'currency_id': currency.id})
