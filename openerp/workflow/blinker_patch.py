# -*- coding: utf-8 -*-

from blinker.base import ANY_ID, Signal
from blinker._utilities import (
    WeakTypes,
    hashable_identity,
    )

_base_names = ['openerp', 'addons', 'signals', 'models']


def receivers_for(self, sender):
    """
    Monkey patch to ensure signals are send just for
    functions that belong to installed modules on
    active database. This allow multi tenancy
    Iterate all live receivers listening for *sender*.
    """
    if self.receivers:
        sender_id = hashable_identity(sender)
        if sender_id in self._by_sender:
            ids = (self._by_sender[ANY_ID] |
                   self._by_sender[sender_id])
        else:
            ids = self._by_sender[ANY_ID].copy()
        for receiver_id in ids:
            receiver = self.receivers.get(receiver_id)
            if receiver is None:
                continue
            if isinstance(receiver, WeakTypes):
                strong = receiver()
                if strong is None:
                    self._disconnect(receiver_id, ANY_ID)
                    continue
                _namespace = strong.func_globals.get('__package__').split('.')
                package = '.'.join(
                    item for item in _namespace if item not in _base_names
                )
                if package not in sender.pool._init_modules:
                    continue
                receiver = strong
            yield receiver

Signal.receivers_for = receivers_for

